import { Given, When, Then } from '@wdio/cucumber-framework';

import LoginPage from '../login.page';
import SecurePage from '../secure.page';
const logger = require( '../config/winston.config');

const pages = {
  login: LoginPage
};

Given(/^I am on the (\w+) page$/, async(page) => {
  logger.info(`Opening ${page} page`);
  await pages[page].open();
});

When(/^I login with (\w+) and (.+)$/, async(username, password) => {
  logger.info(`I login with ${username} username and ${password} password`);
  await LoginPage.login(username, password);
});

Then(/^I should see a flash message saying (.*)$/, async(message) => {
  logger.info(`I validate flash message`);
  await browser.pause(3000);
  await expect(SecurePage.flashAlert).toBeExisting();
  await expect(SecurePage.flashAlert).toHaveTextContaining(message);
});
