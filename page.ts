/**
 * main page object containing all methods, selectors and functionality
 * that is shared across all page objects
 */
export default class Page {

  url: string;

  constructor(url: string) {
    this.url = url;
  }

  /**
   * Opens a sub page of the page
   * @param path path of the sub page (e.g. /path/to/page.html)
   */
  public open() {
    return browser.url(`https://the-internet.herokuapp.com/${this.url}`);
  }
}
